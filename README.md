# fife-fingering

## Description
This is a MuseScore 3 plugin to add fingering for the [fife flute](https://en.wikipedia.org/wiki/Fife_(instrument)) (7 holed version).

The default mode is to write them vertically below the note, like so:\
![centered horizontally](img/demo.png)

```
1
3
|
6
```
means "close finger holes 1 plus 3 to 6 inclusive".

## Installation
Download `FifeFingering.qml` and put in into the [respective](https://musescore.org/en/handbook/3/plugins#installation) folder.
No dependencies needed.

## Configuration
Open `FifeFingering.qml` and search for `property variant`.

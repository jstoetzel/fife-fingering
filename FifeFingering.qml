//=============================================================================
//  Fife Fingering Plugin
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License version 2
//  as published by the Free Software Foundation and appearing in
//  the file LICENCE.GPL
//=============================================================================

import QtQuick 2.0
import MuseScore 3.0

MuseScore {
    version: "0.1"
    description: qsTr("This plugin adds fingering for fife instruments (7 holes)")
    menuPath: qsTr("Plugins.Fife Fingering")

    property variant separator: "\n";
    property variant fingeringSep: "\n";
    property variant offsetY: 5;
    property variant verbose: true;
    property variant baseKey: -2;

    property variant noteTable: {
        60: {fingering: "1|7"},     // D1
        61: {fingering: "1|6"},
        62: {fingering: "1|5"},
        63: {fingering: "1|467"},
        64: {fingering: "1|35|7"},
        65: {fingering: "1|3"},
        66: {fingering: "124|7"},
        67: {fingering: "12"},
        68: {fingering: "13|6"},
        69: {fingering: "1"},
        70: {fingering: "23"},
        71: {fingering: "0"},
        72: {fingering: "2|7"},     // D2
        73: {fingering: "2|6"},
        74: {fingering: "1|5"},
        75: {fingering: "1|46"},
        76: {fingering: "1|356"},
        77: {fingering: "1|3"},
        78: {fingering: "124"},
        79: {fingering: "12"},
        80: {fingering: "13"},
        81: {fingering: "1"},
        82: {fingering: "24|6"},
        83: {fingering: "2|4"},
        84: {fingering: "23"},      // D3
        85: {fingering: "2356"},
        86: {fingering: "1256"},
        87: {fingering: "124"},
        88: {fingering: "1347"},
        89: {fingering: "137"},
        90: {fingering: "3"},
        91: {fingering: "2|5"},
    }

    function getNoteName (tpc) {
        var noteName = "";
        switch (tpc) {
            case -1: noteName = qsTranslate("InspectorAmbitus", "Fbb") + noteName; break;
            case  0: noteName = qsTranslate("InspectorAmbitus", "Cbb") + noteName; break;
            case  1: noteName = qsTranslate("InspectorAmbitus", "Gbb") + noteName; break;
            case  2: noteName = qsTranslate("InspectorAmbitus", "Dbb") + noteName; break;
            case  3: noteName = qsTranslate("InspectorAmbitus", "Abb") + noteName; break;
            case  4: noteName = qsTranslate("InspectorAmbitus", "Ebb") + noteName; break;
            case  5: noteName = qsTranslate("InspectorAmbitus", "Bbb") + noteName; break;
            case  6: noteName = qsTranslate("InspectorAmbitus", "Fb")  + noteName; break;
            case  7: noteName = qsTranslate("InspectorAmbitus", "Cb")  + noteName; break;
            case  8: noteName = qsTranslate("InspectorAmbitus", "Gb")  + noteName; break;
            case  9: noteName = qsTranslate("InspectorAmbitus", "Db")  + noteName; break;
            case 10: noteName = qsTranslate("InspectorAmbitus", "Ab")  + noteName; break;
            case 11: noteName = qsTranslate("InspectorAmbitus", "Eb")  + noteName; break;
            case 12: noteName = qsTranslate("InspectorAmbitus", "Bb")  + noteName; break;
            case 13: noteName = qsTranslate("InspectorAmbitus", "F")   + noteName; break;
            case 14: noteName = qsTranslate("InspectorAmbitus", "C")   + noteName; break;
            case 15: noteName = qsTranslate("InspectorAmbitus", "G")   + noteName; break;
            case 16: noteName = qsTranslate("InspectorAmbitus", "D")   + noteName; break;
            case 17: noteName = qsTranslate("InspectorAmbitus", "A")   + noteName; break;
            case 18: noteName = qsTranslate("InspectorAmbitus", "E")   + noteName; break;
            case 19: noteName = qsTranslate("InspectorAmbitus", "B")   + noteName; break;

            case 20: noteName = qsTranslate("InspectorAmbitus", "F♯")  + noteName; break;
            case 21: noteName = qsTranslate("InspectorAmbitus", "C♯")  + noteName; break;
            case 22: noteName = qsTranslate("InspectorAmbitus", "G♯")  + noteName; break;
            case 23: noteName = qsTranslate("InspectorAmbitus", "D♯")  + noteName; break;
            case 24: noteName = qsTranslate("InspectorAmbitus", "A♯")  + noteName; break;
            case 25: noteName = qsTranslate("InspectorAmbitus", "E♯")  + noteName; break;
            case 26: noteName = qsTranslate("InspectorAmbitus", "B♯")  + noteName; break;
            case 27: noteName = qsTranslate("InspectorAmbitus", "F♯♯") + noteName; break;
            case 28: noteName = qsTranslate("InspectorAmbitus", "C♯♯") + noteName; break;
            case 29: noteName = qsTranslate("InspectorAmbitus", "G♯♯") + noteName; break;
            case 30: noteName = qsTranslate("InspectorAmbitus", "D♯♯") + noteName; break;
            case 31: noteName = qsTranslate("InspectorAmbitus", "A♯♯") + noteName; break;
            case 32: noteName = qsTranslate("InspectorAmbitus", "E♯♯") + noteName; break;
            case 33: noteName = qsTranslate("InspectorAmbitus", "B♯♯") + noteName; break;
            default: noteName = qsTr("?")   + noteName; break;
        }
        return noteName;
    }

    function log(msg) {
        if (verbose) console.log(msg);
    }

    // insert fingeringsep after each char in text
    // e.g. 123 => 1\n2\n3\n
    // TODO: get rid of last occurence
    function addFingeringSeperator(text) {
        return text.replace(/(.{1})/g, "$&" + fingeringSep);
    }

    function getFingering(instrument, notes) {
        var sections = instrument.split(".");
        if (sections == "undefined" || sections.length < 1)
            return "";

        var text = "";
        for (var i = 0; i < notes.length; i++) {
            if (text != "")
                text += separator

            var index = notes[i].pitch + baseKey;
            log("Pitch: " + index + " | NoteName: " + getNoteName(notes[i].tpc));
            if (notes[i].tieBack != null) return "-";
            if (index in noteTable && "fingering" in noteTable[index])
                var separatedString = addFingeringSeperator(qsTr(noteTable[index]["fingering"]));
                text += separatedString;
        }
        return text;
    }

    onRun: {
        if (typeof curScore === 'undefined')
            Qt.quit();
        var cursor = curScore.newCursor();
        var startStaff;
        var endStaff;
        var endTick;
        var fullScore = false;
        cursor.rewind(1);
        if (!cursor.segment) {
            fullScore = true;
            startStaff = 0;
            endStaff = curScore.nstaves - 1;
        } else {
            startStaff = cursor.staffIdx;
            cursor.rewind(2);
            if (cursor.tick == 0) {
                // this happens when the selection includes
                // the last measure of the score.
                // rewind(2) goes behind the last segment (where
                // there's none) and sets tick=0
                endTick = curScore.lastSegment.tick + 1;
            } else {
                endTick = cursor.tick;
            }
            endStaff = cursor.staffIdx;
        }

        for (var staff = startStaff; staff <= endStaff; staff++) {
            cursor.rewind(1)
            cursor.voice = 0;
            cursor.staffIdx = staff;

            if (fullScore)  // no selection
                cursor.rewind(0); // beginning of score

            var instrument = curScore.parts[staff].instrumentId;
            if (!instrument.startsWith("wind")) {
                log("Skipped instrument: " + instrument)
                continue;
            }

            while (cursor.segment && (fullScore || cursor.tick < endTick)) {
                if (!cursor.element || cursor.element.type != Element.CHORD) {
                    cursor.next();
                    continue;
                }

                var fingering = getFingering(instrument, cursor.element.notes);
                if (fingering) {
                    var text = newElement(Element.STAFF_TEXT);
                    text.text = fingering;
                    text.offsetY = offsetY;
                    text.placement = 1; // set to 0 to place above
                    text.align = 2; // center text on reference point
                    cursor.add(text);
                }

                cursor.next();
            }
        }

        Qt.quit();
    }
}
